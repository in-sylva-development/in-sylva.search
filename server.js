'use strict'

const path = require('path')
const fastifyEnv = require('fastify-env')
const fsequelize = require('fastify-sequelize')
const AutoLoad = require('fastify-autoload')
const fastify = require('fastify')({ logger: true, http2: false })
const helmet = require('fastify-helmet')
const api = require('./app/api/search')
const fp = require('./plugins/requestFilter')
const dotenv = require('dotenv');

const schema = {
    type: 'object',
    required: ['PORT', 'NODE_ENV', 'DB_HOST', 'DB_USERNAME', 'DB_PASSWORD', 'DB_DATABASE', 'DB_PORT'],
    properties: {
        PORT: {
            type: 'string',
            default: 6000
        },

        NODE_ENV: { type: 'string' },

        DB_HOST: { type: 'string' },
        DB_USERNAME: { type: 'string' },
        DB_PASSWORD: { type: 'string' },
        DB_DATABASE: { type: 'string' },
        DB_PORT: { type: 'string' },

        ELK_HOST: { type: 'string' },
        ELK_PORT: { type: 'string' },
        ELK_USERNAME: { type: 'string' },
        ELK_PASSWORD: { type: 'string' },

        KEYCLOAK_REALM: { type: 'string' },
        KEYCLOAK_SERVER_URL: { type: 'string' },
        KEYCLOAK_CLIENT_ID: { type: 'string' },

        SEARCH_APP_HOST : { type: 'string' }, 
        SEARCH_APP_PORT : { type: 'string' }, 
    }
}

const options = {
    schema: schema,
    dotenv: true
}

if (process.env.NODE_ENV === 'development') {
    dotenv.config();
}

fastify
    .register(fastifyEnv, options)
    .register(helmet)
    .register(api)
    .register(require('fastify-elasticsearch'), {
        node: `http://${process.env.ELK_HOST}:${process.env.ELK_PORT}`,
        maxRetries: 5,
        requestTimeout: 6000,
        sniffOnStart: true,
        auth: {
            username: process.env.ELK_USERNAME,
            password: process.env.ELK_PASSWORD
        },
    })
    .ready((err) => {
        if (err) console.error(err)
        
        fastify
            .listen(fastify.config.PORT, '0.0.0.0', function (err, address) {
                if (err) {
                    fastify.log.error(err)
                    process.exit(1)
                }

                fastify.log.info(`in-sylva.search service is up and running on  ${address}`)
            })
    })

module.exports = fp(fastify)