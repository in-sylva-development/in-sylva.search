'use strict'

module.exports = function (fastify, opts, done) {
    fastify.get('/healthcheck', {
        schema: {
            response: {
                200: {
                    type: 'object',
                    properties: {
                        text: { type: 'string' }
                    }
                }
            }
        }
    }, async (request, reply) => {
        reply
            .code(200)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send({ text: "search service is running." })
    })

    fastify.post('/search', {}, async (request, reply) => {

        // backlog
        // filter the query result regarding user roles and depend on fields.
        // Save the search history
        // Request body
        /*{
                UserId [string]
                query [object]
                indicesId, [string]
                isSavable  [bool]
        }
        */

        const { query } = request.body.query
        const { indicesId } = request.body
        const { body } = await fastify.elastic.search({
            index: indicesId,
            scroll: '10s',
            body: {
                query
            }
        })
        reply
            .code(200)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send(body)
    })

    fastify.post('/count', {}, async (request, reply) => {
        const { query } = request.body.query
        const { indicesId } = request.body
        const { body } = await fastify.elastic.count({
            index: indicesId,
            body: { query }
        })
        reply
            .code(200)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send(body)
    })

    fastify.post('/scroll-search', {}, async (request, reply) => {
        const { query } = request.body
        const { indicesId } = request.body
        let searchResults = []

        const scrollSearch = fastify.elastic.helpers.scrollSearch({
            index: indicesId,
            body: query
        })
        for await (const result of scrollSearch) {
            if (result && result.body.hits && result.body.hits.hits) {
                result.body.hits.hits.forEach(record => {
                    searchResults.push({
                        "id": `${record._index}_${record._id}`,
                        ...record._source
                    })
                })
            }
        }
        reply
            .code(200)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send(searchResults)
    })

    done()
}
